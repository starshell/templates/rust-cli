//! Fibonacci
//!
//! Calculate the n-th Fibonacci number.
//! Uses Binet's formula for a closed-form solution.
#[macro_use]
extern crate log;
extern crate rust_cli;
extern crate stderrlog;
#[macro_use]
extern crate structopt;

use std::process;
use rust_cli::config::{Config, Options};
use structopt::StructOpt;

fn main() {
    let options = Options::from_args();

    stderrlog::new()
        .module(module_path!())
        .quiet(options.quiet)
        .verbosity(options.verbose)
        .timestamp(stderrlog::Timestamp::Off)
        .color(stderrlog::ColorChoice::Auto)
        .init()
        .unwrap_or_else(|err| {
            eprintln!("Error getting command line options: {}", err);
            process::exit(1);
        });

    let config = Config::new(options).unwrap_or_else(|err| {
        eprintln!("Error getting application configuration: {}", err);
        process::exit(1);
    });

    if let Err(err) = rust_cli::run_fib(config) {
        eprintln!("Application error: {}", err);
        process::exit(1);
    }
}
