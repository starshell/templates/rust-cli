#[macro_use]
extern crate xdg;
pub mod config;
#[macro_use]
extern crate clap;
#[macro_use]
extern crate log;
extern crate stderrlog;
#[macro_use]
extern crate structopt;

use std::error::Error;
use self::config::{Config, Options, SubCommand};

const SQRT_5: f64 = 2.23606797749979;
const PHI: f64 = (1.0 + SQRT_5) / 2.0; // the golden ratio
const PSI: f64 = -(1.0 / PHI);

pub fn run_fib(config: Config) -> Result<(), Box<Error>> {
    match config.options.subcommand {
        SubCommand::Nth { n } => println!("The Fibonacci at index {} is: {}", n, nth_fib(n as i32)),
        SubCommand::Range { start, stop } => {
            println!("The Fibonacci at index {} is: {}", start, stop as i32,)
        }
    };
    Ok(())
}

/// Calculate the n-th Fibonacci number.
///
/// Uses Binet's formula for a closed-form solution. First apply the
/// equation then return the result rounded to the nearest integer.
///
/// # Panics
///
/// Any input over 92 will cause an overflow.
///
fn nth_fib(n: i32) -> i64 {
    if n > 92 {
        panic!();
    }
    let nth = (PHI.powi(n) - PSI.powi(n)) / 5f64.sqrt();
    nth.round() as i64
}

#[cfg(test)]
mod test {
    use super::nth_fib;
    #[test]
    fn nth_zero_to_twenty() {
        assert_eq!(nth_fib(0), 0);
        assert_eq!(nth_fib(1), 1);
        assert_eq!(nth_fib(2), 1);
        assert_eq!(nth_fib(3), 2);
        assert_eq!(nth_fib(4), 3);
        assert_eq!(nth_fib(5), 5);
        assert_eq!(nth_fib(6), 8);
        assert_eq!(nth_fib(7), 13);
        assert_eq!(nth_fib(8), 21);
        assert_eq!(nth_fib(9), 34);
        assert_eq!(nth_fib(10), 55);
        assert_eq!(nth_fib(11), 89);
        assert_eq!(nth_fib(12), 144);
        assert_eq!(nth_fib(13), 233);
        assert_eq!(nth_fib(14), 377);
        assert_eq!(nth_fib(15), 610);
        assert_eq!(nth_fib(16), 987);
        assert_eq!(nth_fib(17), 1597);
        assert_eq!(nth_fib(18), 2584);
        assert_eq!(nth_fib(19), 4181);
        assert_eq!(nth_fib(20), 6765);
    }
}
