extern crate structopt;

use xdg;
use std;
use std::fs;
use std::fs::File;
use std::io::Write;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct Options {
    /// Silence all output
    #[structopt(short = "q", long = "quiet")]
    pub quiet: bool,

    /// Verbose mode (-v, -vv, -vvv, etc)
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    pub verbose: usize,

    #[structopt(subcommand)]
    pub subcommand: SubCommand,
}

#[derive(StructOpt, Debug)]
#[structopt(raw(setting = "structopt::clap::AppSettings::ColoredHelp"))]
pub enum SubCommand {
    /// Print the nth
    #[structopt(name = "nth")]
    Nth {
        /// The nth
        n: u64,
    },

    /// Print a range of the sequence
    #[structopt(name = "range")]
    Range {
        /// The starting point of the desired sequence
        start: i64,

        /// The end of the desired sequence
        stop: i64,
    },
}

pub struct Config {
    pub options: Options,
    pub config_file: File,
    pub data_file: File,
}

impl Config {
    pub fn new(options: Options) -> Result<Config, &'static str> {
        let mut config_file = Config::get_config_file()?;
        let mut data_file = Config::get_data_file()?;
        Ok(Config {
            options,
            config_file,
            data_file,
        })
    }

    fn get_config_file() -> Result<File, &'static str> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix(crate_name!()).unwrap();
        let config_path = xdg_dirs
            .place_config_file("config.yml")
            .expect("cannot create configuration directory");

        if !config_path.exists() {
            match File::create(config_path) {
                Ok(file) => return Ok(file),
                Err(_) => return Err("Unable to create config file"),
            }
        }

        match File::open(config_path) {
            Ok(file) => Ok(file),
            Err(_) => Err("Unable to open config file"),
        }
    }

    fn get_data_file() -> Result<File, &'static str> {
        let xdg_dirs = xdg::BaseDirectories::with_prefix(crate_name!()).unwrap();
        let data_path = xdg_dirs
            .place_data_file("data.ron")
            .expect("cannot create data directory");

        if !data_path.exists() {
            match File::create(data_path) {
                Ok(file) => return Ok(file),
                Err(_) => return Err("Unable to create config file"),
            }
        }

        match File::open(data_path) {
            Ok(file) => Ok(file),
            Err(_) => Err("Unable to open config file"),
        }
    }
}
